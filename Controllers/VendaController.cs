using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly Context.Context _context;

        public VendaController(Context.Context context)
        {
            _context = context;
        }
        
        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Find(id);

            if(venda == null)
                return NotFound();

            return Ok(venda);
        }

        [HttpPost]
        public IActionResult RegistrarVenda(Venda venda)
        {
            var vendedorId = _context.Vendedors.Find(venda.Vendedor);
            venda.Status = EnumStatusVenda.AguardandoPagamaneto;

            if (vendedorId == null)
                return NotFound(new { Erro ="Vendedor inválido!"});
            if (venda.Produtos[0].Nome == null)
                return BadRequest(new { Erro = "A venda deve possuir ao menos um produto!"});
            if (venda.Data == DateTime.MinValue)
                return BadRequest(new { Erro = "A data da venda não pode ser vazia!"});

            venda.Status = EnumStatusVenda.AguardandoPagamaneto;
            _context.Add(venda);
            _context.SaveChanges();

            return CreatedAtAction(nameof(ObterPorId), new { id = venda.Id }, venda);;
        }

        [HttpPut("{id}")]
        public IActionResult AtualizarVenda(int id, EnumStatusVenda status)
        {
            var vendaBanco = _context.Vendas.Find(id);
            var erro = "Não foi possível alterar o status da venda!";

            if (vendaBanco == null)
                return NotFound();
            
            switch(vendaBanco.Status)
            {
                case EnumStatusVenda.AguardandoPagamaneto:
                    if (!(status == EnumStatusVenda.PagamentoAprovado || status == EnumStatusVenda.Cancelada))
                        return BadRequest(new { Erro = erro});
                break;
                case EnumStatusVenda.PagamentoAprovado:
                    if (!(status != EnumStatusVenda.EnviadoTransportadora || status != EnumStatusVenda.Cancelada))
                        return BadRequest(new { Erro = erro});
                break;
                case EnumStatusVenda.EnviadoTransportadora:
                if (!(status != EnumStatusVenda.Entregue))
                        return BadRequest(new { Erro = erro});
                break;
            }

            vendaBanco.Status = status;
            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            return CreatedAtAction(nameof(ObterPorId), new { id = vendaBanco.Id }, vendaBanco);
        }

    }
}