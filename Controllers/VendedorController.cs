using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly Context.Context _context;
        public VendedorController(Context.Context context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var vendedor = _context.Vendedors.Find(id);

            if(vendedor == null)
                return NotFound();

            return Ok(vendedor);
        }
        [HttpPost]
        public IActionResult RegistrarVendedor(Vendedor vendedor)
        {
            var cpf = vendedor.Cpf;
            var email = vendedor.Email;
            var cpfUnico = _context.Vendedors.Where(x => x.Cpf == cpf);

            if (cpf.Length != 11)
                return BadRequest(new { Erro = "CPF inválido!" });
            
            if (!email.Contains("@") || !email.Contains("."))
                return BadRequest(new { Erro = "Email inválido!" });
            
            if (cpfUnico == null)
                return BadRequest(new { Erro = "Já existe um vendedor cadastrado com esse CPF!" });

            _context.Add(vendedor);
            _context.SaveChanges();

            return CreatedAtAction(nameof(ObterPorId), new { id = vendedor.Id }, vendedor);
        }
    }
}